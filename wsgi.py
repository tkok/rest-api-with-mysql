#!/usr/bin/env python
"""
Example for launch server

Run as simple_server
`python wsgi.py`

Run with gunicorn
`gunicorn -w 2 wsgi:app`

"""
from wsgiref import simple_server

from restmysql.app import server


def app(environ, response) -> list:
    return server(environ, response)


if __name__ == '__main__':
    with simple_server.make_server('', 8800, server) as httpd:
        httpd.serve_forever()
