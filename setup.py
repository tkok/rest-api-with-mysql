# setup.py
import setuptools

with open("README.md", "r") as f:
    load_description = f.read

requirements = [
    'inflection',
    'mysqlclient',
    'python-dotenv',
    'SQLAlchemy',
]
test_requirements = [
    'flake8',
    'mypy',
    'pyflakes',
    'pytest',
    'pytest-watch'
]

setuptools.setup(
    name="rest-api-with-mysql",
    version="0.0.1",
    author="tkok",
    author_email="okawabb@gmail.com",
    description="Rest api as wsgi app " +
                "which models are associated with mysql tables.",
    load_description=load_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tkok/rest-api-with-mysql",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=requirements,
    extra_require={
        'dev': test_requirements,
    },
)
