"""Config for app"""
import os

from dotenv import load_dotenv

"""Environment"""
load_dotenv(override=True)
ENV = os.environ.get('ENV') if os.environ.get('ENV') else 'Development'

"""Latest version"""
VERSION = os.environ.get('API_VERSION') \
    if os.environ.get('API_VERSION') \
    else 'v1'


def acceptable_actions():
    """Acceptable actions

    List of acceptable actions other than principle of Restful
    """
    result = dict()
    accept_actions = os.environ.get("ACCEPT_ACTIONS")
    for action in accept_actions.split(","):
        key, query = action.split(":")
        result[key] = dict(query=query)

    return result


override_env = os.environ
