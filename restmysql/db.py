"""Module for db"""
from typing import Type

from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.orm import sessionmaker, scoped_session

from .config import ENV, override_env


class Setting(object):
    """Setting for sqlalchemy engine
    """
    USER = override_env.get('DB_USER')
    PASS = override_env.get('DB_PASS')
    HOST = override_env.get('DB_HOST')
    PORT = override_env.get('DB_PORT')
    NAME = override_env.get('DB_NAME')

    def __init__(self):
        """Constructor
        """

    def __str__(self):
        return f"USER={self.USER},PASS={self.PASS},HOST={self.HOST}" \
            f",PORT={self.PORT},NAME={self.NAME}"

    @property
    def engine(self) -> Type[Engine]:
        """Engine by sqlalchemy

        Returns:
            obj: Instance from sqlalchemy.create_engine()
        """
        db = f"mysql://{self.USER}:{self.PASS}" \
            f"@{self.HOST}:{self.PORT}/{self.NAME}?charset=utf8"

        echo = True if ENV == 'Production' else False

        return create_engine(db, encoding="utf-8", echo=echo)


def get_session():
    return scoped_session(
        sessionmaker(
            autocommit=False,
            autoflush=False,
            bind=Setting().engine
        )
    )
