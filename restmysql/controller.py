"""Controller for api

Controller has roles to make relation between means of request to
corresponding.
This module has functions and classes to handle it.
"""
from urllib.parse import urlparse
from typing import Union, Any, Tuple
from wsgiref.util import request_uri
import json

from sqlalchemy import MetaData
from sqlalchemy.exc import OperationalError

from restmysql.config import acceptable_actions, VERSION
from restmysql.db import Setting
from restmysql.action import Query, ActionError


def query_props_from_url(url: str) -> dict:
    """Parse url and set props for query as dict

    Url pattern is '/{version}/{model}/{id|action}'
    Examples are:
        GET  /v1/items  Find all from items
        GET  /v1/items/1 Find by id=1 from items
        POST /v1/items/search Find by query from items
        POST /v1/items Insert new records with param in body
        PUT  /v1/items/1 Update record of id=1 with param in body
        DELETE /v1/items/1 Delete record of id=1
    Args:
        url (str): String of url
    Returns:
        dict: Props for query which has attributes "version", "model",
            "action", "id".
    """
    p = urlparse(url)
    tmp = [''] * 3
    for k, v in enumerate(p.path.split("/")[1:4]):
        tmp[k] = v

    version, model, action = tmp

    if action.isdigit():
        num = int(action)
        action = ''
    else:
        num = 0

    return {"version": version, "model": model, "action": action, "id": num}


def query_type(query_props: dict, http_method="GET") -> str:
    """Decide query type by query_props and http method.
    query types:
        Find, Insert, Update, Delete
    Args:
        query_props (dict): Result of `query_props_from_url()`
    Returne:
        str: 'Find'(Default)|'Insert'|'Update'|'Delate'
    """

    if http_method == "PUT":
        return "Update"
    elif http_method == "DELETE":
        return "Delete"
    elif http_method == "POST":
        if "action" in query_props and \
           query_props["action"] in acceptable_actions().keys():
            return acceptable_actions()[query_props["action"]]["query"]
        else:
            return "Insert"
    else:
        return "Find"


class Validation(object):
    """Validation for property in query_props
    """

    def __init__(self, query_props: dict):
        """Constructor

        Args:
            query_props (dict): Returns from `query_props_from_url()`
        """
        self.query_props = query_props

    def _version(self) -> Union[bool, None]:
        if self.query_props["version"] == VERSION:
            return True
        else:
            version = self.query_props["version"] \
                if self.query_props["version"] else "It"
            raise QueryPropsValidationError(
                1,
                f'{version} is invalid version.'
            )

    def _action(self) -> Union[bool, None]:
        if self._id() and self.query_props["id"] > 0:
            return True
        elif self.query_props["action"] in acceptable_actions().keys():
            return True
        else:
            action_name = self.query_props["action"] \
                if self.query_props["action"] else "It"
            raise QueryPropsValidationError(
                4,
                f'{action_name} is not registered action.'
            )

    def _id(self) -> Union[bool, None]:
        if isinstance(self.query_props["id"], int):
            return True
        else:
            raise QueryPropsValidationError(
                3,
                f'{self.query_props["id"]} is invalid id.'
            )

    def _model(self) -> Union[bool, None]:
        meta = MetaData()
        try:
            meta.reflect(bind=Setting().engine)
        except OperationalError as err:
            raise QueryPropsValidationError(2, str(err))

        if self.query_props["model"] in meta.tables:
            return True
        else:
            model = self.query_props["model"] \
                if self.query_props["model"] else "It"
            raise QueryPropsValidationError(
                2,
                f'{model} is not existed model.'
            )

    def run(self) -> Union[bool, None]:
        """Run validation for query properties

        Returns:
            bool: if each valid check does not raise error, it returns True.
        Raises:

        """
        return self._version() and self._model() and self._id() \
            and self._action()


class ControllerError(Exception):
    def __init__(self, code: int, message: str):
        self.code = code
        message = "[{0}] {1}".format(self.code, message)
        super().__init__(message)


class QueryPropsValidationError(ControllerError):
    pass


class Request(object):
    """Request class which handles http request headers and variables
    for interpretation."""

    def __init__(self, environ: dict) -> None:
        self._environ = environ

    def uri(self) -> str:
        """Return full request uri."""
        return request_uri(self._environ)

    def method(self) -> str:
        """Return request method
        If unexpected method is passed, it returns "GET" in default.
        """
        request_method = self._environ.get("REQUEST_METHOD")
        return request_method if isinstance(request_method, str) else "GET"

    def body(self) -> Any:
        """get request body

        Extract body as json if it exists.
        """
        body = self._environ.get("wsgi.input")
        content_length = self._environ.get("CONTENT_LENGTH")
        if content_length and body:
            return json.loads(body.read(int(content_length)))
        else:
            return None

    def query_params(self) -> dict:
        """Return query params extracted from json body.
        Which are only expected properties defined
        in `restmysql.action.Query` dataclass as fields.

        Returns:
            dict: Params for query which is defined in Query dataclass.
        """
        params: dict = {}
        if self.method() not in ["POST", "PUT"]:
            return params
        b = self.body()
        if not b:
            return params

        exclude_keys = ["model", "record_id"]
        titles = [
            i for i in Query.__annotations__.keys() if i not in exclude_keys
        ]
        for title in titles:
            if title in b:
                params[title] = b[title]

        return params


class Response(object):
    """Response class prepares corresponding http response.

    Attributes:
        _status_code (str): Http status code as string like "200 OK".
        _body (dict): Formatted dict for response body
    """
    status_codes: dict = {
        400: "Bad Request",
        200: "OK"
    }

    def __init__(self):
        self._status_code: str = ""
        self._body: dict = {}

    def error(
            self,
            status_code: int,
            err_code: int,
            err: Union[ControllerError, ActionError]) -> None:
        """Assign corresponding properties

        Args:
            status_code (int): For Http status code
                which defined as `self.status_codes`
            err_code (int): Specific error code to identify for App.
            err: Called Error instance to make error message.
        """
        self._status_code = f"{status_code} {self.status_codes[status_code]}"
        self._body = {
            "code": err_code,
            "message": str(err)
        }

    def success(self, body: dict) -> None:
        """Assign properties as success

        Args:
            body (dict): Response body for success
        """
        status_code = 200
        self._status_code = f"{status_code} {self.status_codes[status_code]}"
        self._body = body

    def content_type_header(self) -> Tuple[str, str]:
        """Return content_type of header
        At this point it directly assign only json with utf-8.
        """
        return ('Content-type', 'application/json; charset=utf-8')

    def headers(self) -> list:
        """Return customized response headers as list
        At this point it only assigns content type header.
        """
        return [self.content_type_header()]

    def status_code(self) -> str:
        """Return assigned `_status_code` """
        return self._status_code

    def response_body(self) -> list:
        """Return response body formatted for Http response
        which is based on assigned `_body`.
        """
        return [json.dumps(self._body, default=str).encode('utf-8')]
