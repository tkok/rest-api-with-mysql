#!/usr/bin/env python
from restmysql.controller import (
    Request,
    query_props_from_url,
    query_type,
    Validation,
    QueryPropsValidationError,
    Response
)
from restmysql.action import ActionError, Query
import restmysql.action
from restmysql.db import get_session
from restmysql.controller import ControllerError


class App(object):
    """App class

    App class as server which has methods and instances of Request and Response
    to receive http request and return response as Json sequentially.

    Attributes:
        _environ (dict): env properties received as WSGIApplication
        _request (Request): Instance of Request
        _response (Response): Instance of Response
    """

    def __init__(self, environ: dict) -> None:
        self._environ = environ

        self._request: Request = Request(environ)
        self._response: Response = Response()

    def get_response(self) -> Response:
        """Return Response instance"""
        return self._response

    def validate_query_props(self) -> bool:
        """Validation for request of uri"""
        props = query_props_from_url(self._request.uri())
        v = Validation(props)
        try:
            result = v.run()
            if not result:
                raise QueryPropsValidationError(0, "Invalid request uri.")
            else:
                return True
        except QueryPropsValidationError as err:
            self._response.error(400, 2001, err)
            return False

    def exec_query(self) -> bool:
        """Sequential execution includes db query for request"""
        props = query_props_from_url(self._request.uri())
        action_name = query_type(props, self._request.method())
        # Extract body as json
        query_params = self._request.query_params()
        # db session
        session = get_session()
        # instance Query
        # if not exists it fails.
        try:
            action = getattr(restmysql.action, action_name)
        except AttributeError:
            self._response.error(
                400,
                3001,
                ControllerError(0, "Invalid uri is requested.")
            )
            return False
        try:
            query = Query(props["model"], props["id"], **query_params)
        except ValueError as err:
            self._response.error(400, 3002, ControllerError(0, str(err)))
            return False

        # instance Action
        try:
            action_ins = action(query, session)
        except ActionError as err:
            self._response.error(400, 3003, err)
            return False

        try:
            result = action_ins.run()
            if isinstance(result, bool):
                self._response.success({"result": True})
            else:
                self._response.success(action_ins.records_as_dict(result))

        except ActionError as err:
            self._response.error(400, 3004, err)

        return True


def server(environ, respond) -> list:
    app = App(environ)

    if app.validate_query_props():
        app.exec_query()

    response = app.get_response()
    respond(response.status_code(), response.headers())
    return response.response_body()
