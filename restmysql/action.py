"""action module"""
from abc import ABCMeta, abstractmethod
from typing import Dict, List, Union, Optional, Type, Tuple, Any
from dataclasses import dataclass, field
from datetime import datetime

from sqlalchemy import MetaData, text, Table, or_, update as upd_query
from sqlalchemy.sql.elements import BinaryExpression, TextClause
from sqlalchemy.orm import scoped_session, Query as Q
from sqlalchemy.sql.schema import Column
from inflection import singularize
from sqlalchemy.exc import OperationalError

from restmysql.db import Setting, get_session


@dataclass
class Query:
    """Dataclass for query

    Validation check for each assigned field type is executed
    when instantiate Query with args.
    Optional field which type is defined with typing will be validated
    by using default_factory.

    Args:
        model (str): Target model name
        record_id (int, optional): Target id
        relations: (list, optional): Related models
        new_params: (dict, optional): Use for insert and update
        conditions: (dict, optional): Condition for search
        orders: (dict, optional): Orders for result of list
        fields: (dict, optional): Fields for result of list
        offset: (list, optional): Offset for query

    Examples:
        relations = ["exhibit_lists", "auction_datas"]
        new_params = {
            "exhibit_lists": {"delete_flag": 0, "age": 20}
        }
        conditions = {
            "exhibit_lists": {
                "delete_flag": 0,
                "age": {"operator":">","value":20},
                "created": {
                    "operator":">", "type":"isodate",
                    "value":"2020-01-02T03:04:05+09:00"
                },
                "or": [
                    {"flag": 1},
                    {"flag": {"operator": ">=", "value": 3}}
                ]
            }
        }
        orders = {"auction_datas":["id", "-created"]}
        fields = {"exhibit_lists":["id","name"], "auction_datas":["title"]}
        offset = [0, 10]    // Limit 0, 10
    """
    model: str
    record_id: int = 0
    relations: List[str] = field(default_factory=list)
    new_params: Dict[str, Dict[str, Union[int, str]]
                     ] = field(default_factory=dict)
    conditions: Dict[str, Dict[str, Union[int, str, dict, list]]] \
        = field(default_factory=dict)
    orders: Dict[str, List[str]] = field(default_factory=dict)
    fields: Dict[str, List[str]] = field(default_factory=dict)
    offset: List[int] = field(default_factory=list)

    def _validate(self) -> str:
        err = ""
        for name, def_field in self.__dataclass_fields__.items():
            assigned_type = type(getattr(self, name))
            if "typing" in str(def_field.type):
                defined_type = def_field.default_factory
            else:
                defined_type = def_field.type
            if assigned_type != defined_type:
                return f"{assigned_type.__name__} is " \
                    + f"unexpected type for {name}."

        return err

    def __post_init__(self):
        res = self._validate()
        if res:
            raise ValueError(res)


class Action(metaclass=ABCMeta):
    """Metaclass of query action for db

    Attributes:
        dbmeta (dict): All metadata of tables
        query (Query): Dataclass of Query
        session  (scoped_session): Session for execution
    """

    def __init__(
            self,
            query: Query,
            session: Type[scoped_session] = None) -> None:
        """Constructor
        Initially it assigns meta infos of all tables.
        """
        meta = MetaData()
        try:
            meta.reflect(Setting().engine)
        except OperationalError as err:
            raise ActionError(100, str(err))

        self.dbmeta = meta
        self.query = query
        self.model = self.model_by_name(self.query.model)

        if session:
            self.session = session
        else:
            self.session = get_session()

    def model_by_name(self, name: str) -> Type[Table]:
        """Return `sqlalchemy.Table` by `name`

        Returns:
            Type[sqlalchemy.Table]: Table object by `name`
        Raises:
            ActionError: if `name` of model is not found in db.
        """
        if name not in self.dbmeta.tables:
            raise ActionError(100, f'{name} is not existed in db.')

        return self.dbmeta.tables[name]

    def set_last_query(self, statement: str) -> None:
        """Setter for last_statement"""
        self.last_statement = statement

    def debug_last_query(self) -> None:
        """Just output last_statement"""
        print(self.last_statement)

    def get_column(self, model: str,
                   column: str) -> Union[Type[Column], None]:
        """Get column

        Returns:
            `sqlalchemy.sql.schema.Column`: Column object
            None: if column is not found.
        """
        m = self.model_by_name(model)
        if column in m.c:
            return m.c[column]
        else:
            return None

    def records_as_dict(self, records: Union[Any, List]) -> Union[Dict, List]:
        """Return result records by query as dict
        """
        if isinstance(records, list):
            return [
                rec._asdict() if hasattr(rec, "_asdict") else rec
                for rec in records
            ]
        else:
            if hasattr(records, "_asdict"):
                return records._asdict()
            else:
                return records

    @abstractmethod
    def run(self):
        pass


class Find(Action):
    """Class Find

    It implements methods to execute find query based on orders as `Query`
    for database.
    It extends metaclass `Action` which __init__ needs instance of `Query`.
    """

    def __repr__(self) -> str:
        return f'Find(Query("{self.model}", record_id={self.query.record_id}))'

    def run(self) -> Union[None, Q, List[Optional[str]]]:
        """Execute find query

        Execute query with conditions and return target records.
        if query has specific record_id, it will return one record.

        Returns:
            Dict[str, str]: `sqlalchemy.util._collections.result`
                Result of a record by query
            List[Dict[str, str]]: Result of records by query
            None: If target records are not found.
        """
        model = self.model
        session = self.session
        if (self.query.record_id > 0):
            # Find by id
            """if record_id is assigned, it should return single record."""
            q = session.query(model).\
                filter_by(id=self.query.record_id)
            super().set_last_query(str(q.statement))
            record = q.one_or_none()
            if not record:
                raise FindError(101, "Resource is not found.")
            return record
        else:
            q = session.query(model)
            target_model_names = [model.fullname]

            if (len(self.query.relations) > 0):
                # JOIN TABLES
                for relation in self.query.relations:
                    if relation not in self.dbmeta.tables:
                        raise FindError(
                            101, f"{relation} is not existed in db.")
                    else:
                        r_model = self.model_by_name(relation)
                        f_key = self.__foreign_key(relation)
                        q = q.join(
                            r_model,
                            model.c[f_key] == r_model.c.id
                        )
                        target_model_names.append(r_model.fullname)

            # Find with conditions
            for model_name in target_model_names:
                if model_name in self.query.conditions:
                    for k, v in self.query.conditions[model_name].items():
                        fil = self.__filter(k, v, model_name)
                        q = q.filter(fil)

            # ORDER
            for m, orders in self.query.orders.items():
                for order in orders:
                    if order[0] == "-":
                        column_name = order[1:]
                        column = self.__get_column(m, column_name)
                        if column is not None:
                            q = q.order_by(column.desc())

                    else:
                        column = self.__get_column(m, order)
                        q = q.order_by(column)

            # LIMIT
            if len(self.query.offset) == 2:
                q = q.limit(self.query.offset[0]).offset(self.query.offset[1])
            elif len(self.query.offset) == 1:
                q = q.limit(self.query.offset[0])
            else:
                q = q.limit(100)

            # FIELDS
            fields = self.__fields()
            if fields:
                q = q.with_entities(*fields)
            super().set_last_query(str(q.statement))
            records = q.all()
            if not records:
                raise FindError(101, "Resource is not found.")
            return records

    def __foreign_key(self, related_model: str) -> Union[str, None]:
        """Search foreign key in model

        Returns:
            str: Name of foreigh key
        Raises:
            FindError
        """
        model = self.model
        key_name = f"{singularize(related_model)}_id"
        if key_name not in model.c:
            raise FindError(
                102,
                f"Expected key '{key_name}' is not existed in {model.fullname}"
            )
        else:
            return key_name

    def __filter(self, key: str,
                 value: Union[
                     str, int,
                     Dict[str, str],
                     Dict[str, int],
                     List[Dict]
                 ],
                 model: str = '') \
        -> Union[Type[BinaryExpression],
                 Type[TextClause], None]:
        """Return arg for query filter

        Returns:
            BinaryExpression: If value is str or int.
            TextClause: If value contains operator symbol.
        Raises:
            FindError: If value contains unexpected type of value.
        """
        model = model if model else self.query.model
        if not isinstance(value, str) and \
                not isinstance(value, int) and \
                not isinstance(value, dict) and \
                not isinstance(value, list):
            # Raise if value is unexpected.
            raise FindError(
                103, f"Condition can not accept type of {type(value)}"
            )
        if isinstance(value, dict):
            # Case value is dict.
            tests = [
                isinstance(j, str) or
                isinstance(j, int) for j in value.values()]
            if False in tests:
                # dict value should be str or int.
                raise FindError(
                    103,
                    f"Condition {str(value)} can not accept unexpected value")
            elif "operator" in value and "value" in value:
                # dict value should include operator and value keys.
                if isinstance(value["value"], str) or \
                        isinstance(value["value"], int):
                    value_for_condition = value["value"]
                else:
                    raise FindError(
                        103,
                        f"{str(value['value'])} should be assigned " +
                        "as int or str for query with operator."
                    )
                if "type" in value:
                    """
                    if value has type, it needs to convert value to specific
                    type
                    """
                    if value["type"] == "isodate":
                        try:
                            if not isinstance(value["value"], str):
                                raise ValueError

                            dt = datetime.fromisoformat(value["value"])
                        except ValueError:
                            raise FindError(
                                103,
                                f"{str(value)} should be assigned as str " +
                                "as iso date format."
                            )
                        else:
                            value_for_condition = dt.strftime(
                                "%Y-%m-%d %H:%M:%S"
                            )
                if isinstance(value_for_condition, str):
                    # if value_for_condition is str it is needed to quote.
                    value_for_condition = f'"{value_for_condition}"'

                op = f'{model}.{key} ' +\
                    f'{value["operator"]} {value_for_condition}'

                return text(op)
            else:
                raise FindError(
                    103,
                    f"Condition {str(value)} can not accept unexpected value"
                )
        elif key == "or" and isinstance(value, list):
            # Case that OR filters
            or_list = []
            for clause in value:
                for k, v in clause.items():
                    # Nested or_ raise error.
                    if k == "or":
                        raise FindError(
                            103, "Nested OR condition can not be accepted."
                        )
                    else:
                        f = self.__filter(k, v, model=model)
                        or_list.append(f)
            return or_(*or_list)
        else:
            try:
                return self.model_by_name(model).c[key] == value
            except KeyError:
                raise FindError(
                    103, f"Unexpected {model}.{key}={value}"
                )

    def __fields(self) -> List[Column]:
        """Fields list for query

        Returns:
            List[`sqlalchemy.sql.schema.Column`]: List of Columns
                if relation tables are joined, each column is labeled formatted
                as 'model_name.column_name'.
        """
        all_fields = []
        for model, fields in self.query.fields.items():
            for field_name in fields:
                column = self.__get_column(model, field_name)
                if column is not None:
                    if self.query.relations:
                        column = column.label(f"{model}.{field_name}")
                    all_fields.append(column)

        return all_fields

    def __get_column(self, model: str,
                     column: str) -> Union[Type[Column], None]:
        """Wrapper of Action.get_column()

        Returns:
            Action.get_column()
        Raises:
            FindError: If return of Action.get_column() is None.
        """
        col = self.get_column(model, column)

        if col is None:
            raise FindError(
                104, f"{column} is not existed in {model}"
            )
        else:
            return col


class Update(Action):
    """Class Update

    It implements methods to execute update query based on orders as `Query`
    for database.
    It extends metaclass `Action` which __init__ needs instance of `Query`.
    Update required only two params `record_id` and `new_params` of `Query`.
    """

    def __validation(self) -> Union[bool, None]:
        """Validation for query properties
        In this case query.record_id and query.new_params are required.
        For new_params check that each column exists in model.

        Returns:
            bool: If validation is ok.
        Raises:
            UpdateError: If failed in validation process.
        """
        model = self.model
        if self.query.record_id < 1:
            raise UpdateError(201, "record_id is required.")
        if not self.query.new_params or \
                self.query.model not in self.query.new_params:
            raise UpdateError(201, "new_params are required.")
        for k, _ in self.query.new_params[self.query.model].items():
            if k not in model.c:
                raise UpdateError(
                    201, f"{k} is not existed in {model.fullname}"
                )

        if self.query.relations:
            for relation in self.query.relations:
                try:
                    self.model_by_name(relation)
                except FindError:
                    raise UpdateError(
                        201, f"{relation} is not existed."
                    )
                key_name = f"{singularize(relation)}_id"
                foreign_key = self.get_column(self.query.model, key_name)
                if foreign_key is None:
                    raise UpdateError(
                        201, f"{key_name} is not existed in {self.query.model}"
                    )

        return True

    def __rollback_and_raise(self, code_and_msg: Tuple[int, str]) -> None:
        """Session rollback and then raise UpdateError with `arg_for_error`"""
        self.session.rollback()
        raise UpdateError(*code_and_msg)

    def run(self) -> bool:
        """Update target record with new params.

        Process requires properties of query are record_id and new_params.
        If Failed to update in process, it raises UpdateError.
        If it is success, finally session is commit.
        """
        if self.__validation():
            try:
                target = Find(
                    Query(self.query.model, record_id=self.query.record_id),
                    self.session
                ).run()
                if target is None or isinstance(target, list):
                    raise FindError(202, "")
            except FindError:
                raise UpdateError(
                    202,
                    f"{self.query.model}.id={self.query.record_id}"
                    " is not found."
                )

            try:
                stmt = upd_query(self.model)\
                    .where(self.model.c.id == self.query.record_id)\
                    .values(self.query.new_params[self.query.model])
                super().set_last_query(str(stmt))
                result = self.session.execute(stmt)
                if result.rowcount != 1:
                    raise UpdateError(203, "rowcount!=1")
            except (UpdateError, BaseException) as err:
                self.__rollback_and_raise(
                    (
                        203,
                        "Failed to update "
                        f"{self.query.model}.id={self.query.record_id}"
                        f" with {str(self.query.new_params[self.query.model])}"
                        f" err: {err}"
                    )
                )

            # for relation
            for relation in self.query.relations:
                f_key = f"{singularize(relation)}_id"
                record_id = target._asdict()[f_key]
                # update
                try:
                    related_model = self.model_by_name(relation)
                    stmt = upd_query(related_model)\
                        .where(related_model.c.id == record_id)\
                        .values(self.query.new_params[relation])
                    result = self.session.execute(stmt)
                    super().set_last_query(str(stmt))
                    if result.rowcount != 1:
                        raise UpdateError(204, "rowcount!=1")
                except (FindError, UpdateError, BaseException) as err:
                    self.__rollback_and_raise(
                        (
                            204,
                            "Failed to update"
                            f"{relation}.id={record_id}"
                            f" with {str(self.query.new_params[relation])}"
                            f" err: {err}"
                        )
                    )

            self.session.commit()
            return True

        return False


class ActionError(Exception):
    def __init__(self, code: int, message: str):
        self.code = code
        message = "[{0}] {1}".format(self.code, message)
        super().__init__(message)


class FindError(ActionError):
    pass


class UpdateError(ActionError):
    pass
