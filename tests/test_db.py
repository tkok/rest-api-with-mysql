"""Test for db module"""
import os

from sqlalchemy.engine.base import Engine

from restmysql.db import Setting


class TestSetting(object):

    def test_expected_properties(self):
        s = Setting()
        assert s.USER == os.environ["DB_USER"]
        assert s.PASS == os.environ["DB_PASS"]

    def test_expected_engine(self):
        s = Setting()

        assert isinstance(s.engine, Engine)
