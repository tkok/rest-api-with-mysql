"""Test for App in app.py"""
from os import environ
import re
from restmysql.action import ActionError

import pytest

from restmysql.app import App
from restmysql.controller import Request, Response


class TestApp(object):

    @pytest.fixture
    def mock_env(self, mocker):
        return mocker.MagicMock(spec=environ)

    def test_constructor(self, mock_env):

        app = App(mock_env)
        assert isinstance(app._request, type(Request(mock_env)))
        assert isinstance(app._response, type(Response()))
        assert app._environ == mock_env

    def test_validate_query_props_pattern_for_error(self, mocker, mock_env):
        mock_valid = mocker.MagicMock()
        mock_valid.run.return_value = False
        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.Validation', return_value=mock_valid)
        app = App(mock_env)
        res = app.get_response()
        assert not app.validate_query_props()
        assert res.status_code() == "400 Bad Request"
        assert re.search(
            r'\[0\] Invalid request uri',
            res.response_body()[0].decode())

    def test_validate_query_props_pattern_for_success(self, mocker, mock_env):
        mock_valid = mocker.MagicMock()
        mock_valid.run.return_value = True
        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.Validation', return_value=mock_valid)
        app = App(mock_env)
        assert app.validate_query_props()

    def test_exec_query_should_return_false_if_unexpected_action_request(
            self, mocker, mock_env):

        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mock_req.method.return_value = "DELETE"
        mock_req.query_params.return_value = {}
        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.get_session', return_value=None)

        app = App(mock_env)
        assert not app.exec_query()
        res = app.get_response()
        assert res.status_code() == "400 Bad Request"
        assert re.search(
            r'\[0\] Invalid uri is requested',
            res.response_body()[0].decode())

    def test_exec_query_should_return_false_if_unexpected_props_passed(
        self, mocker, mock_env
    ):
        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mock_req.method.return_value = "GET"
        mock_req.query_params.return_value = {"relations": 1}
        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.get_session', return_value=None)

        app = App(mock_env)
        assert not app.exec_query()
        res = app.get_response()
        assert res.status_code() == "400 Bad Request"
        assert re.search(
            r'\[0\] int is unexpected type for relations',
            res.response_body()[0].decode())

    def test_exec_query_should_return_false_if_action_instance_raise_error(
        self, mocker, mock_env
    ):
        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mock_req.method.return_value = "GET"
        mock_req.query_params.return_value = {}
        mock_meta = mocker.MagicMock()
        mock_meta.tables = {"model_a": 1}
        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.get_session', return_value={})
        mocker.patch(
            'restmysql.app.query_props_from_url',
            return_value={
                "model": "model_b", "id": 1})
        mocker.patch('restmysql.action.MetaData', return_value=mock_meta)

        app = App(mock_env)
        app.exec_query()
        res = app.get_response()
        assert res.status_code() == "400 Bad Request"
        assert re.search(
            r'\[100\] model_b is not existed in db',
            res.response_body()[0].decode())

    def test_exec_query_should_return_false_if_action_raise_error(
        self, mocker, mock_env
    ):
        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mock_req.method.return_value = "GET"
        mock_req.query_params.return_value = {}
        mock_find = mocker.MagicMock()
        mock_find.run.side_effect = ActionError(999, "err!")

        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.get_session', return_value={})
        mocker.patch('restmysql.action.Find', return_value=mock_find)

        app = App(mock_env)
        app.exec_query()
        res = app.get_response()
        assert res.status_code() == "400 Bad Request"
        assert re.search(
            r'\[999\] err!',
            res.response_body()[0].decode())

    def test_exec_query_should_return_true_with_expected_response_result(
        self, mocker, mock_env
    ):
        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mock_req.method.return_value = "PUT"
        mock_req.query_params.return_value = {}
        mock_action = mocker.MagicMock()
        mock_action.run.return_value = True

        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.get_session', return_value={})
        mocker.patch('restmysql.action.Update', return_value=mock_action)

        app = App(mock_env)
        app.exec_query()
        res = app.get_response()
        assert res.status_code() == "200 OK"
        assert re.search(
            r'"result": true',
            res.response_body()[0].decode())

    def test_exec_query_should_return_true_with_expected_response_body(
        self, mocker, mock_env
    ):
        mock_req = mocker.MagicMock()
        mock_req.uri.return_value = "test"
        mock_req.method.return_value = "GET"
        mock_req.query_params.return_value = {}
        mock_action = mocker.MagicMock()
        dummy = {"test": 1}
        mock_action.run.return_value = dummy
        mock_action.records_as_dict.return_value = dummy

        mocker.patch('restmysql.app.Request', return_value=mock_req)
        mocker.patch('restmysql.app.get_session', return_value={})
        mocker.patch('restmysql.action.Find', return_value=mock_action)

        app = App(mock_env)
        app.exec_query()
        res = app.get_response()
        assert res.status_code() == "200 OK"
        assert re.search(
            r'"test": 1',
            res.response_body()[0].decode())
