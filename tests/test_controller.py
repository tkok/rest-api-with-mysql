"""Test for controller"""
from contextlib import nullcontext as does_not_raise
import json

import pytest

from restmysql.controller import (
    query_props_from_url,
    query_type,
    Validation,
    MetaData,
    QueryPropsValidationError,
    Request,
    Response
)
from restmysql.config import acceptable_actions


class TestParseUrl(object):
    @pytest.mark.parametrize(
        "pattern", [
            (
                "https://test.com/v1/items/1",
                {"version": "v1", "model": "items", "action": "",
                 "id": 1}
            ),
            (
                "https://test.com/v1/items/search",
                {"version": "v1", "model": "items", "action": "search",
                    "id": 0}
            ),
            (
                "test.com/v1/",
                {"version": "v1", "model": "", "action": "", "id": 0}
            ),
            (
                "http://test.com/v2/e/123/456",
                {"version": "v2", "model": "e", "action": "", "id": 123}
            ),
        ]
    )
    def test_for_patterns(self, pattern):
        p = query_props_from_url(pattern[0])
        assert p == pattern[1]


class TestQueryType(object):
    @pytest.mark.parametrize(
        "pattern", [
            ({}, "GET", "Find"),
            ({"action": "search"}, "POST", "Find"),
            ({"action": "search"}, "PUT", "Update"),
            ({"action": ""}, "DELETE", "Delete"),
            ({"action": ""}, "DEL", "Find"),
        ]
    )
    def test_for_patterns(self, pattern):
        q = query_type(pattern[0], pattern[1])
        assert q == pattern[2]


class TestValidation(object):
    def test_expected_result_for_version(self):
        q = {"version": "v2"}
        v = Validation(q)
        with pytest.raises(QueryPropsValidationError, match=r"\[1\] v2 is") \
                as err:
            v._version()
        assert err.type is QueryPropsValidationError

        q = {"version": "v1"}
        v = Validation(q)
        assert v._version()

    def test_expected_result_for_action(self):
        q = {"id": 0, "action": "abc"}
        v = Validation(q)
        with pytest.raises(QueryPropsValidationError, match=r"\[4\] abc is") \
                as err:
            v._action()
        assert err.type is QueryPropsValidationError

        action_name = list(acceptable_actions().keys())[0]

        q = {"id": 0, "action": action_name}
        v = Validation(q)
        assert v._action()

    @pytest.mark.parametrize(
        "pattern,expectation", [
            ({"id": "aa"},
                pytest.raises(QueryPropsValidationError, match=r"\[3\]")),
            ({"id": "12"}, pytest.raises(QueryPropsValidationError)),
            ({"id": 0}, does_not_raise()),
            ({"id": 123}, does_not_raise()),
        ]
    )
    def test_expected_result_for_id(self, pattern, expectation):
        v = Validation(pattern)
        with expectation:
            assert v._id()

    def test_expected_result_for_model(self, mocker):
        mock = mocker.Mock(spec=MetaData)
        mock.reflect = mocker.MagicMock()
        mock.tables = dict({
            "items": 123,
            "users": 123,
        })
        mocker.patch('restmysql.controller.MetaData', return_value=mock)

        v = Validation({"model": "abc"})

        with pytest.raises(QueryPropsValidationError, match=r"\[2\] abc is") \
                as err:
            v._model()
        assert err.type is QueryPropsValidationError

        mock.reflect.assert_called_once()

        v = Validation({"model": "items"})
        assert v._model()

    @pytest.mark.parametrize("pattern,expectation", [
        ({"version": "v2", "model": "items"}, pytest.raises(
            QueryPropsValidationError, match=r"\[1\]")
         ),
        (
            {"version": "v1", "model": "123"},
            pytest.raises(
                QueryPropsValidationError, match=r"\[2\]"
            )
        ),
        (
            {"version": "v1", "model": "items", "id": "0",
             "action": "a"},
            pytest.raises(
                QueryPropsValidationError, match=r"\[3\]"
            )
        ),
        (
            {"version": "v1", "model": "items", "id": 0,
             "action": "a"},
            pytest.raises(
                QueryPropsValidationError, match=r"\[4\]"
            )
        ),
        (
            {"version": "v1", "model": "items", "id": 10,
             "action": "aa"},
            does_not_raise()
        ),
        (
            {"version": "v1", "model": "items", "id": 0,
             "action": "search"},
            does_not_raise()
        ),
    ])
    def test_run_with_pattern(self, mocker, pattern, expectation):
        mock = mocker.Mock(spec=MetaData)
        mock.reflect = mocker.MagicMock()
        mock.tables = dict({
            "items": 123,
            "users": 123,
        })
        mocker.patch('restmysql.controller.MetaData', return_value=mock)

        v = Validation(pattern)
        with expectation:
            assert v.run()


class TestRequest(object):

    def test_constructor(self):
        env = {"a": 1}
        r = Request(env)
        assert r._environ == env

    def test_query_params_should_return_expected_values(self, mocker):
        r = Request(1)
        r.method = mocker.MagicMock(name="method", return_value="POST")

        r.body = mocker.MagicMock(
            name="body", return_value={
                "relations": ["model_a", "model_b"],
                "offset": [10, 20],
                "condition": 123
            }
        )
        res = r.query_params()

        assert res == {
            "relations": ["model_a", "model_b"],
            "offset": [10, 20],
        }

        r.method.assert_called_once()
        r.body.assert_called_once()


class TestResponse(object):
    def test_error_should_assign_properties(self):
        r = Response()
        with pytest.raises(KeyError):
            r.error(11, 22, QueryPropsValidationError(33, "err!"))

        r.error(400, 1, QueryPropsValidationError(2, "err!"))
        res = r.status_code()
        assert res == "400 Bad Request"

        res = r.response_body()
        expected = {"code": 1, "message": "[2] err!"}
        assert res == [json.dumps(expected).encode("utf8")]

        r.success({"res": True})
        assert r.status_code() == "200 OK"
        assert r.response_body() == [json.dumps({"res": True}).encode("utf8")]
