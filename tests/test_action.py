"""test for action"""
import re

from sqlalchemy import Table, Column, text, or_
from sqlalchemy.types import Integer, String
from sqlalchemy.orm import scoped_session
import pytest

from restmysql.action import (
    Action,
    ActionError,
    Query,
    Find,
    MetaData,
    FindError,
    Update,
    UpdateError
)


class TempAction(Action):

    def run(self):
        pass


class TestQuery(object):
    def test_instance_should_raise_error_if_validation_error(self):

        with pytest.raises(ValueError, match=r"int is unexpected"):
            Query(1)
        with pytest.raises(ValueError, match=r"list is unexpected"):
            Query("model", [1])
        with pytest.raises(ValueError,
                           match=r"int is unexpected type for relations"):
            Query("model", 1, relations=1)
        with pytest.raises(ValueError,
                           match=r"dict is unexpected type for offset"):
            Query("model", 1, offset={"test": 1})


class TestAction(object):
    def test_constructor(self, mocker):
        """Constructor assign dbmeta"""
        mock = mocker.Mock(spec=MetaData)
        mock.reflect = mocker.MagicMock()
        fixtures = dict({
            "exhibit_lists": {},
            "auction_datas": 123
        })
        mock.tables = fixtures
        mocker.patch('restmysql.action.MetaData', return_value=mock)
        q = Query("exhibit_lists")
        a = TempAction(q)

        assert a.dbmeta.tables == fixtures

    def test_records_as_dict_should_return_expected_records(self, mocker):
        q = Query("exhibit_lists")
        a = TempAction(q)

        # Value without _asdict()
        records = 123
        res = a.records_as_dict(records)
        assert records == res

        # Value with _asdict()
        mock = mocker.MagicMock()
        mock._asdict.return_value = "result"
        res = a.records_as_dict(mock)
        assert res == "result"

        # Values with _asdict() in list
        mock2 = mocker.MagicMock()
        mock2._asdict.return_value = "result2"
        records = [mock, mock2, 123]
        res = a.records_as_dict(records)
        assert res == ["result", "result2", 123]


class TestFind(object):
    def setup_method(self, method):
        # print(dir(method))
        pass

    def mock_for_metadata(self, mocker):
        mock = mocker.Mock(spec=MetaData)
        mock.reflect = mocker.MagicMock()
        meta = MetaData()
        fixtures = dict({
            "exhibit_lists": Table(
                "exhibit_lists", meta,
                Column('id', Integer),
                Column('auction_data_id', Integer),
                Column('title', String(200)),
                Column('delete_flag'),
            ),
            "auction_datas": "model_a",
            "yahauc_sold_items": Table(
                "yahauc_sold_items", meta,
                Column('id', Integer),
                Column('exhibit_list_id', Integer),
            )
        })
        mock.tables = fixtures
        return mock

    def test_find_one(self, mocker):
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))

        result = {"a": 123}
        session_mock = mocker.Mock()
        session_mock.query.return_value.filter_by.\
            return_value.one_or_none.return_value = result
        mocker.patch('restmysql.action.get_session', return_value=session_mock)
        q = Query(
            "auction_datas",
            record_id=1
        )
        f = Find(q)
        res = f.run()
        assert res == result
        session_mock.query.assert_called_with("model_a")

    def test_foreign_key_error_case(self, mocker):
        """If expected key is not existed, it raise expected error."""
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))

        f = Find(Query("exhibit_lists", record_id=1))
        with pytest.raises(FindError, match=r"\[102\]"):
            f._Find__foreign_key("test")

    def test_foreign_key(self, mocker):
        """In success, it should return foreign key as str"""
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))

        f = Find(Query("exhibit_lists", record_id=1))
        res = f._Find__foreign_key("auction_datas")
        assert res == "auction_data_id"

    def test_filter_raise_error_unexpected_value(self, mocker):
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))

        f = Find(Query("exhibit_lists", record_id=1))

        with pytest.raises(FindError, match=r"\[103\]"):
            f._Find__filter("1", [1, 2])

        with pytest.raises(FindError, match=r"\[103\]"):
            f._Find__filter("1", 1.0)

        with pytest.raises(FindError, match=r"\[103\]"):
            f._Find__filter("1", {"a": 1, "b": [1, 2]})

        with pytest.raises(FindError, match=r"\[103\] Nested OR"):
            f._Find__filter("or", [{"delete_flag": 1}, {"or": []}])

        f._Find__filter("1", {"operator": ">", "value": 2})

    def test_filter_return_expected_value(self, mocker):
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))

        f = Find(Query("exhibit_lists", record_id=1))
        res = f._Find__filter(
            "test", {"operator": ">", "value": 20}
        )

        assert str(res) == str(text("exhibit_lists.test > 20"))

        res = f._Find__filter("auction_data_id", "hello")

        assert str(res) == str(f.model.c["auction_data_id"] == "hello")

        or_condi = [
            {"delete_flag": 0},
            {"auction_data_id": 123},
            {"auction_data_id": {"operator": "<", "value": 0}}
        ]
        res = f._Find__filter("or", or_condi)

        list_or = []
        for condi in or_condi:
            for k, v in condi.items():
                r = f._Find__filter(k, v, model="exhibit_lists")
                list_or.append(r)

        assert str(res) == str(or_(*list_or))

    def test_fields_raise_error(self, mocker):
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))

        fields = {
            "exhibit_lists": ["t_id"]
        }

        f = Find(Query("exhibit_lists", record_id=1, fields=fields))

        with pytest.raises(FindError, match=r"\[104\]"):
            f._Find__fields()

    def test_fields_return_expected_value(self, mocker):
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))

        fields = {
        }
        f = Find(Query("exhibit_lists", record_id=1, fields=fields))
        r = f._Find__fields()
        assert not r

        fields = {
            "exhibit_lists": ["auction_data_id"],
            "yahauc_sold_items": ["id"]
        }
        m = self.mock_for_metadata(mocker)

        f = Find(Query("exhibit_lists", record_id=1, fields=fields))
        r = f._Find__fields()
        assert str(r[0]) == str(m.tables["exhibit_lists"].c["auction_data_id"])
        assert str(r[1]) == str(m.tables["yahauc_sold_items"].c["id"])

    def test_fields_with_relations(self, mocker):
        pass

    def test_find_with_conditions(self, mocker):
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=self.mock_for_metadata(mocker))
        result = [{"a": 123}]
        session_mock = mocker.Mock()
        session_mock.query.return_value\
            .filter.return_value\
            .order_by.return_value\
            .limit.return_value\
            .all.return_value = result
        mocker.patch('restmysql.action.get_session', return_value=session_mock)
        q = Query(
            "exhibit_lists",
            offset=[10],
            orders={"exhibit_lists": ["-auction_data_id"]},
            conditions={"exhibit_lists": {"delete_flag": 0}}
        )
        f = Find(q)
        res = f.run()
        assert res == result

    def test_find_with_joined_model(self, mocker):
        fixtures = self.mock_for_metadata(mocker)
        mocker.patch(
            'restmysql.action.MetaData',
            return_value=fixtures)

        session_mock = mocker.Mock(spec=scoped_session)
        session_mock.query.return_value\
            .join.return_value\
            .filter.return_value\
            .filter.return_value\
            .order_by.return_value\
            .limit.return_value\
            .with_entities.return_value\
            .all.return_value = [1, 2]

        mocker.patch('restmysql.action.get_session', return_value=session_mock)
        q = Query(
            "yahauc_sold_items",
            relations=["hello"]
        )
        f = Find(q)
        with pytest.raises(FindError, match=r"\[101\]"):
            f.run()

        q = Query(
            "yahauc_sold_items",
            relations=["exhibit_lists"],
            offset=[10],
            orders={"exhibit_lists": ["-auction_data_id"]},
            conditions={
                "exhibit_lists": {"delete_flag": 0},
                "yahauc_sold_items": {"id": {"operator": ">=", "value": 10}}
            },
            fields={"exhibit_lists": ["id"]}
        )
        f = Find(q)
        res = f.run()
        assert res == [1, 2]
        ee = fixtures.tables["exhibit_lists"]
        yy = fixtures.tables["yahauc_sold_items"]

        args = session_mock.query.return_value.join.call_args
        assert ee == args[0][0]
        assert str(yy.c.exhibit_list_id == ee.c.id) == str(args[0][1])

        args2 = session_mock.query.return_value.join.return_value\
            .filter.call_args
        assert str(args2[0][0]) == str(text(f"{yy.c.id} >= 10"))


class TestUpdate(object):
    def setup_method(self, method):
        # print(dir(method))
        pass

    @pytest.fixture
    def mock_metadata(self, mocker):
        mock = mocker.Mock(spec=MetaData)
        mock.reflect = mocker.MagicMock()
        meta = MetaData()
        fixtures = dict({
            "exhibit_lists": Table(
                "exhibit_lists", meta,
                Column('id', Integer),
                Column('auction_data_id', Integer),
                Column('title', String(200)),
                Column('delete_flag'),
            ),
            "auction_datas": "model_a",
            "yahauc_sold_items": Table(
                "yahauc_sold_items", meta,
                Column('id', Integer),
                Column('exhibit_list_id', Integer),
                Column('packing_id', Integer),
                Column('complete', Integer),
            ),
            "packings": Table(
                "packings", meta,
                Column('id', Integer)
            )
        })
        mock.tables = fixtures
        return mock

    @pytest.fixture
    def mock_query_no_relation(self):
        return Query(
            "yahauc_sold_items",
            record_id=3,
            new_params={
                "yahauc_sold_items": {
                    "complete": 1
                },
            },
        )

    @pytest.fixture
    def mock_query_with_relation(self):
        return Query(
            "yahauc_sold_items",
            record_id=3,
            new_params={
                "yahauc_sold_items": {
                    "complete": 1
                },
                "exhibit_lists": {
                    "delete_flag": 0
                }
            },
            relations=["exhibit_lists"]
        )

    def test_validation_raise_error(self, mocker, mock_metadata):
        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        q = Query(
            "yahauc_sold_items",
        )
        u = Update(q)
        with pytest.raises(UpdateError, match=r"\[201\] record_id"):
            u._Update__validation()

        q = Query(
            "yahauc_sold_items",
            record_id=2
        )
        u = Update(q)
        with pytest.raises(UpdateError, match=r"\[201\] new_params are"):
            u._Update__validation()

        q = Query(
            "yahauc_sold_items",
            record_id=3,
            new_params={"yahauc_sold_items": {
                "delete_flag": 1
            }}
        )
        u = Update(q)
        with pytest.raises(UpdateError, match=r"\[201\] delete_flag is not"):
            u._Update__validation()

        q = Query(
            "yahauc_sold_items",
            record_id=3,
            new_params={
                "yahauc_sold_items": {
                    "complete": 1
                },
            },
            relations=["tests"]
        )
        u = Update(q)
        with pytest.raises(ActionError, match=r"\[100\] tests is not"):
            u._Update__validation()

        q = Query(
            "yahauc_sold_items",
            record_id=3,
            new_params={
                "yahauc_sold_items": {
                    "complete": 1
                },
            },
            relations=["auction_datas"]
        )
        u = Update(q)
        with pytest.raises(UpdateError, match=r"\[201\] auction_data_id is"):
            u._Update__validation()

    def test_update_raise_error_if_find_is_error(
        self, mocker, mock_metadata, mock_query_no_relation
    ):
        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.side_effect = FindError(1, "")
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        session_mock = mocker.MagicMock(spec=scoped_session)
        u = Update(mock_query_no_relation, session_mock)
        with pytest.raises(UpdateError,
                           match=r"\[202\] yahauc_sold_items.id=3"):
            u.run()

    def test_update_raise_error_if_find_return_none(
            self, mocker, mock_metadata, mock_query_no_relation):
        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.return_value = None
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        session_mock = mocker.MagicMock(spec=scoped_session)
        u = Update(mock_query_no_relation, session_mock)
        with pytest.raises(UpdateError,
                           match=r"\[202\] yahauc_sold_items.id=3"):
            u.run()
        # rollback is unnecessary
        session_mock.rollback.assert_not_called()

    def test_update_raise_error_if_upd_query_raise_error(
        self, mocker, mock_metadata, mock_query_no_relation
    ):
        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.return_value = True
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        mocker.patch(
            'restmysql.action.upd_query',
            side_effect=BaseException("warning!"))

        session_mock = mocker.MagicMock(spec=scoped_session)
        u = Update(mock_query_no_relation, session_mock)
        with pytest.raises(UpdateError,
                           match=r"warning"):
            u.run()

        session_mock.rollback.assert_called()

    def test_update_raise_error_if_upd_query_not_update(
        self, mocker, mock_metadata, mock_query_no_relation
    ):
        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.return_value = True
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        mocker.patch('restmysql.action.upd_query')

        session_mock = mocker.MagicMock(spec=scoped_session)
        session_mock.execute.return_value.rowcount = 0

        u = Update(mock_query_no_relation, session_mock)
        with pytest.raises(UpdateError,
                           match=r"rowcount\!=1"):
            u.run()

        session_mock.rollback.assert_called()

    def test_update_expect_call_session_commit(
        self, mocker, mock_metadata, mock_query_no_relation
    ):
        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.return_value = True
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        upd_mock = mocker.MagicMock()
        upd_mock.where.values.return_value = 1
        mocker.patch(
            'restmysql.action.upd_query',
            return_value=upd_mock
        )

        session_mock = mocker.MagicMock(spec=scoped_session)
        session_mock.execute.return_value.rowcount = 1
        u = Update(mock_query_no_relation, session_mock)
        u.run()

        find_mock.run.assert_called_once()
        upd_mock.where.return_value.values.assert_called_once_with(
            {"complete": 1}
        )
        session_mock.commit.assert_called()

    def test_update_with_relation_raise_error_if_upd_query_is_error(
        self, mocker, mock_metadata, mock_query_with_relation
    ):

        returned_mock = mocker.MagicMock()
        returned_mock._asdict.return_value = {"exhibit_list_id": 123}

        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.return_value = returned_mock
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        upd_mock = mocker.MagicMock()
        upd_mock.where.values.return_value = 1
        mocker.patch(
            'restmysql.action.upd_query',
            side_effect=[upd_mock, FindError(999, "dummy_error")]
        )

        session_mock = mocker.MagicMock(spec=scoped_session)
        session_mock.execute.return_value.rowcount = 1
        with pytest.raises(UpdateError, match=r"err: \[999\] dummy_error"):
            u = Update(mock_query_with_relation, session_mock)
            u.run()

        session_mock.rollback.assert_called()

    def test_update_with_relation_raise_error_if_failed_to_update(
        self, mocker, mock_metadata, mock_query_with_relation
    ):
        returned_mock = mocker.MagicMock()
        returned_mock._asdict.return_value = {"exhibit_list_id": 123}

        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.return_value = returned_mock
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        mocker.patch('restmysql.action.upd_query')

        session_mock = mocker.MagicMock(spec=scoped_session)

        r_mock1 = mocker.MagicMock()
        r_mock1.rowcount = 1
        r_mock2 = mocker.MagicMock()
        r_mock2.rowcount = 2
        session_mock.execute.side_effect = [
            r_mock1, r_mock2
        ]

        u = Update(mock_query_with_relation, session_mock)
        with pytest.raises(UpdateError, match=r"err: \[204\] rowcount\!=1"):
            u.run()
        session_mock.rollback.assert_called()

    def test_update_with_relation_expect_call_session_commit(
        self, mocker, mock_metadata, mock_query_with_relation
    ):
        returned_mock = mocker.MagicMock()
        returned_mock._asdict.return_value = {"exhibit_list_id": 123}

        mocker.patch('restmysql.action.MetaData', return_value=mock_metadata)
        find_mock = mocker.MagicMock(spec=Find)
        find_mock.run.return_value = returned_mock
        mocker.patch('restmysql.action.Find', return_value=find_mock)
        upd_mock = mocker.MagicMock()
        upd_mock.where.values.return_value = 1
        mocker.patch(
            'restmysql.action.upd_query',
            return_value=upd_mock
        )

        session_mock = mocker.MagicMock(spec=scoped_session)

        session_mock.execute.return_value.rowcount = 1

        u = Update(mock_query_with_relation, session_mock)
        u.run()

        session_mock.commit.assert_called_once()
        # Check args for upd_query
        called_with = upd_mock.where.return_value.values.call_args_list
        expected = [(mocker.call({"complete": 1})),
                    (mocker.call({"delete_flag": 0}))]
        assert called_with == expected

        called_with = upd_mock.where.call_args_list
        assert re.match("yahauc_sold_items\\.id =", str(called_with[0][0][0]))
        assert re.match("exhibit_lists\\.id =", str(called_with[1][0][0]))
