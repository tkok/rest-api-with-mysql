"""Test action with fixtures of test db
"""
import os
from datetime import datetime

import pytest

from restmysql.action import Query, Find, Update


@pytest.mark.skipif(
    os.environ.get("DB_HOST") != "127.0.0.1", reason="Test for only local"
)
class TestFindWithDb(object):

    def setup_method(self):
        pass

    @pytest.fixture
    def fixture_p1(self, session_scope):
        session_scope.execute(
            "CREATE TABLE IF NOT EXISTS test_a "
            "(id int primary key, name varchar(10), "
            "flag tinyint(1) not null default 0, "
            "created datetime not null default CURRENT_TIMESTAMP);"
            "CREATE TABLE IF NOT EXISTS test_b "
            "(id int primary key, test_a_id int not null, "
            "flag tinyint(1) not null default 0);"
        )

        session_scope.execute(
            "INSERT INTO test_a (id, name) VALUES (1, 'my name');"
            "INSERT INTO test_a (id, name, flag) VALUES (2, 'hello', 1);"
            "INSERT INTO test_a (id, name, flag, created) "
            " VALUES (3, 'world', 2, '2020-01-01 11:22:33');"
            "INSERT INTO test_b (id, test_a_id) VALUES (1, 2);"
        )

        yield session_scope

        session_scope.execute(
            "DROP TABLE test_a;"
            "DROP TABLE test_b;"
        )

    def test_basic_find_query(self, fixture_p1):
        """
        Query
            SELECT id, name from test_a WHERE flag=1
        """
        a = fixture_p1.execute("SELECT *  FROM test_a WHERE id=1")
        assert a.fetchone().id == 1

    def test_find_with_id_should_return_one_expected_record(self, fixture_p1):

        n = datetime(2021, 5, 19, 14, 48)
        rec_id = 1
        q = Query("test_a", rec_id)
        f = Find(q, fixture_p1)
        res = f.run()
        record_as_dict = res._asdict()

        assert n <= record_as_dict['created']
        assert record_as_dict['id'] == rec_id

    def test_find_with_query_should_return_expected_records(self, fixture_p1):
        c = {"test_a": {"flag": {"operator": ">=", "value": 1}}}
        orders = {"test_a": ["id"]}
        q = Query("test_a", conditions=c, orders=orders)
        f = Find(q, fixture_p1)

        res = f.run()

        assert res[0]._asdict()['name'] == 'hello' and \
            res[0]._asdict()['id'] == 2

        assert res[1]._asdict()['name'] == 'world'

        c = {
            "test_a": {
                "created": {
                    "operator": "<",
                    "type": "isodate",
                    "value": "2020-01-01T11:22:34"}}}
        q = Query("test_a", conditions=c)
        f = Find(q, fixture_p1)
        res = f.run()
        assert len(res) == 1
        assert res[0]._asdict()["id"] == 3

    def test_find_with_joinquery_should_return_expected_records(
            self, fixture_p1):
        rel = ["test_a"]
        c = {"test_b": {"id": 1}, "test_a": {"flag": 1}}
        fields = {
            "test_b": ["id", "test_a_id"],
            "test_a": ["name", "flag", "created"]
        }
        q = Query("test_b", conditions=c, relations=rel, fields=fields)
        f = Find(q, fixture_p1)
        r = f.run()
        rec_as_dict = r[0]._asdict()
        assert rec_as_dict['test_b.id'] == 1
        assert rec_as_dict['test_b.test_a_id'] == 2
        assert rec_as_dict['test_a.name'] == 'hello'

    def test_update_should_modify_target(self, fixture_p1):
        q = Query(
            "test_b",
            record_id=1,
            new_params={"test_b": {"flag": 10}}
        )
        u = Update(q, fixture_p1)
        u.run()
        f = Find(q, fixture_p1)
        res = f.run()
        assert res._asdict()['flag'] == 10

        # Update related table at the same time.
        q2 = Query(
            "test_b",
            record_id=1,
            relations=["test_a"],
            new_params={
                "test_b": {"flag": 11},
                "test_a": {"flag": 12}
            }
        )
        u = Update(q2, fixture_p1)
        u.run()

        # Check updated columns
        q3 = Query("test_b",
                   relations=["test_a"],
                   conditions={"test_b": {"id": 1}},
                   fields={"test_b": ["flag"], "test_a": ["flag"]}
                   )
        f = Find(q3, fixture_p1)
        res = f.run()[0]._asdict()
        assert res["test_b.flag"] == 11 and res["test_a.flag"] == 12

        # Other records are not affected.
        f = Find(Query("test_a", record_id=3), fixture_p1)
        other_rec = f.run()._asdict()
        assert other_rec["flag"] != 12
