import os

import pytest
from dotenv import load_dotenv

from restmysql.db import Setting, get_session
load_dotenv(override=True)


@pytest.fixture(scope="session", autouse=True)
def tests_setup_and_teardown():
    os.environ['ENV'] = "Test"
    os.environ['AUTH'] = "true"


@pytest.fixture
def session_scope(scope="module"):
    Setting.NAME = os.environ.get('DB_TEST') or 'test_db'
    session = get_session()
    yield session
    session.rollback()
