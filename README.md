# rest-api-with-mysql
Rest style web application with models in mysql database.

## Description
This web application which can accept rest base request uri and body as json  
and then returning corresponding response as json to client.  
Expected models are defined as tables of mysql database.  
For example request uri `example.com/v1/items/1` with GET method means that  
it needs one record which id is 1 in items in general.  
In this case it requires table named 'items' which has at least a column  
named 'id' in mysql database.

## Latest version
0.0.1-alpha  

## Features
### 0.0.1
- `GET`, `POST` methods
- `Find` and `Update` query
- `application/json` header for both request and response

## Install
`pip install git+https://gitlab.com/tkok/rest-api-with-mysql.git`  

## Usage
How to run with gunicorn
- Make virtualenv `python -m venv venv` and activate it.
- Install this package with pip
- Install gunicorn with pip
- Make `wsgi.py` example is below.
- Make `.env` based on `.env.default`.
- Make `gunicorn.py` for gunicorn configuration.
- Launch app with gunicorn `gunicorn wsgi:app --config gunicorn.py`

wsgi.py
```
#!/usr/bin/env python
"""
Launch app with gunicorn
"""
from restmysql.app import server

def app(env, res) -> list:
    return server(env, res)
```

## Settings
For application it requires to set properties as ENV variables.  
`.env.default` can be used as default file.
| Property name | Description |
| ---- | ---- |
| DB_USER | User for mysql |
| DB_PASS | Password for mysql |
| DB_HOST | Hostname |
| DB_PORT | Port number |
| DB_NAME | Database name |
| DB_TEST | Database name for test |
| ACCEPT_ACTIONS | Acceptable customized action names
| API_VERSION | Api version using in uri like `v1`


`ACCEPT_ACTIONS` is specific parameters which can be used to accept customized  
action name.  
For example uri of `v1/items/search` can be accepted with  
`ACCEPT_ACTIONS=search:Find` for Find query which can use query params as json  
body by POST method at the same time.  
It can include multiple options like `action_a:Find,action_b:Update`.


## Endpoint patterns
Basic uri pattern should be https://domain.com/v1/[MODEL]/[OPTION] which  
basically follows the pattern of REST API URI.

### MODEL
It can be corresponding table name in mysql database.  

### OPTION
It can be assigned number or string. Number will be specific id of record 
which target MODEL has. String will be customized action name which is defined  
in env.

### HTTP Request method
Implemented list at this version.
| Method | Correspond action
| --- | --- |
| GET | Find |
| POST | Find, Update |
| PUT | Update |

### HTTP Response status code
Expected http status codes are:
- 400 Bad Request
- 200 OK

Unexpected result includes empty will be returned as json with 400 status code.

### Request params as json body
It can be passed as Request body by json format.  
It can has some defined property which means conditions for sql query.
#### Example
```
{
    "relations" : ["groups", "items"],
    "new_params" : {
        "users": {"delete_flag": 0, "age": 20}
    },
    "conditions" : {
        "users": {
            "delete_flag": 0,
            "age": {"operator":">","value":20},
            "created": {
                "operator":">", "type":"isodate",
                "value":"2020-01-02T03:04:05+09:00"
            },
            "or": [
                {"flag": 1},
                {"flag": {"operator": ">=", "value": 3}}
            ]
        }
    },
    "orders" : {"users":["id", "-created"]},
    "fields" : {"users":["id","name"], "titles":["title"]},
    "offset" : [0, 10],
}
```
#### conditions (object)
Condition for query which expresses mysql where clause.

| Examples | Query |
| --- | --- |
| {"model_a": {"status": 1, "flag": 0}} | WHERE model_a.status = 1 AND model_a.flag = 0 |
| {"model_a": {"status": {"operator": ">=", "value": 2}}} | WHERE model_a.status >= 2 |
| {"model_a": "created": {"operator":">", "type":"isodate","value":"2020-01-02T03:04:05+09:00"}} | WHERE model_a.created > '2020-01-02 03:04:05' |
| {"model_a": "or": [{"flag": 1},{"flag": {"operator": ">=", "value": 3}}] | WHERE model_a.flag = 1 OR model_a.flag >= 3

#### relations (array)
Assign related model for main model which express join clause.

| Examples | Query |
| --- | --- |
| ["model_b"] | INNER JOIN model_b ON model_a.model_b_id = model_b.id

#### orders (object)
Append order clause to query.

| Examples | Query |
| --- | --- |
| {"model_a": ["id"], "model_b": ["-created"]} | ORDER BY model_a.id, model_b.created DESC

#### fields (object)
Listing fields which must be included in response.
| Examples | Query |
| --- | --- |
| {"model_a": ["id", "title"]} | SELECT model_a.id, model_a.title FROM model_a

#### offset (array)
Append offset clause to query.
| Examples | Query |
| --- | --- |
| [10, 20] | LIMIT 10 OFFSET 20
| [20] | LIMIT 20
| [] or null(default) | LIMIT 100

#### new_params (object)
Assign new parameters to update which express set clause.  
It can be only used for Update query.
| Examples | Query |
| --- | --- |
| {"model_a", {"inventory": 5, "flag": 0}} | SET model_a.inventory = 5, model_a.flag = 0 |

### Success response
Response should be returned as json body with http status code 200.  
For find query it should be records, for update it should be simple.
#### Examples
```
# Find one
{"id": 1, "title": "apple"}

# Search
[
    {"model_a.id": 1, "model_b.title": "apple"}, 
    {"model_a.id": 2, "model_b.title": "orange"}, 
]

# Update
{"result": True}
```

### Error response
Response should be returned as json body with http status code 400.  
It has confirmed properties "code" and "message".  
"message" includes minor error code and detailed message.
```
{
    "code": 1001,
    "message": "[10] Invalid query."
}
```
#### Error Code
| Code | Description |
| --- | --- |
| 2001 |Validation error for request uri.|
| 3001 |Request uri could not call unexpected action.|
| 3002 |Request uri and params could not prepare query.|
| 3003 |Expected model is not existed.|
| 3004 |Error is occurred in execution proccess.|
